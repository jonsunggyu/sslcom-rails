FactoryBot.define do
  factory :reminder_trigger do
    sequence(:name) { |n| n }
  end
end
