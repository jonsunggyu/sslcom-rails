# frozen_string_literal: true

require 'capybara/rspec'
require 'capybara-screenshot/rspec'
require 'webmock/rspec'
require 'webdrivers'

## GITHUB ACTIONS CONFIG START
if ENV.fetch('CI') { false }
  Selenium::WebDriver::Chrome::Service.driver_path = "/usr/bin/chromedriver"

  chrome_args = { args: %w[window-size=1280,1024 headless disable-gpu] }
  selenium_options = Selenium::WebDriver::Chrome::Options.new(chrome_args)

  Capybara.register_driver :chrome do |app|
    Capybara::Selenium::Driver.new(
      app,
      browser: :chrome, options: selenium_options
    )
  end

  Capybara::Screenshot.register_driver(:chrome) do |driver, path|
    driver.browser.save_screenshot(path)
  end

  driver = :chrome
else
  driver = :selenium_chrome
end
## END ##

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.before(:each, type: :system) do
    driven_by :rack_test
  end

  config.before(:each, type: :system, js: true) do
    driven_by :selenium_chrome_headless
  end

  config.after(:each, type: :feature) do |example|
    screenshot_and_save_page if example.exception.present?
  end

  config.mock_with :mocha

  # This option will default to `:apply_to_host_groups` in RSpec 4 (and will
  # have no way to turn it off -- the option exists only for backwards
  # compatibility in RSpec 3). It causes shared context metadata to be
  # inherited by the metadata hash of host groups and examples, rather than
  # triggering implicit auto-inclusion in groups with matching metadata.
  config.shared_context_metadata_behavior = :apply_to_host_groups

  # The settings below are suggested to provide a good initial experience
  # with RSpec, but feel free to customize to your heart's content.
  #   # This allows you to limit a spec run to individual examples or groups
  #   # you care about by tagging them with `:focus` metadata. When nothing
  #   # is tagged with `:focus`, all examples get run. RSpec also provides
  #   # aliases for `it`, `describe`, and `context` that include `:focus`
  #   # metadata: `fit`, `fdescribe` and `fcontext`, respectively.
  #   config.filter_run_when_matching :focus
  #
  # Allows RSpec to persist some state between runs in order to support
  # the `--only-failures` and `--next-failure` CLI options. We recommend
  # you configure your source control system to ignore this file.
  config.example_status_persistence_file_path = 'spec/examples.txt'
  config.profile_examples = 10
  config.order = :random
end

Capybara.default_driver = :selenium_chrome
Capybara.javascript_driver = driver
Capybara.server = :puma, { Silent: true }
Capybara.default_max_wait_time = ENV['CONTINUOS_INTEGRATION'] == true ? 10 : 5
Capybara::Screenshot.prune_strategy = :keep_last_run
Capybara::Screenshot.webkit_options = { width: 1920, height: 1080 }
Capybara::Screenshot.autosave_on_failure = true
