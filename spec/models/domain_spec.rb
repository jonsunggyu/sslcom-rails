# frozen_string_literal: true
require 'rails_helper'

describe Domain do
  it_behaves_like 'it filters on domain'
end
