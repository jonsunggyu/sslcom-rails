class ShoppingCartPage < SitePrism::Page
  element :remove_button, '#remove_0'
  element :clear_cart_button, '#clear_cart'
  element :shop_more_button, '#shop_more_img'
  element :checkout_button, '#checkout_img'
  element :ev_domains, '#certificate_order_certificate_contents_attributes_0_additional_domains'
  element :next_button, '#next_submit'
end
