module PaypalExpressHelper
  def get_setup_purchase_params(cart, request, params)
    @subtotal, @shipping, @total = get_totals(cart, params)
    @items                       = get_items(cart)
    credit_and_discount params
    load_funds(params) unless params[:reprocess_ucc] || params[:monthly_invoice]
    
    @return_url_params = {
      action: 'purchase',
      ssl_slug: params[:ssl_slug],
      only_path: false,
      deduct_order: params[:deduct_order],
      order_description: params[:order_description]
    }
    
    get_domains_adjustments
    
    if params[:monthly_invoice]
      @return_url_params.merge!({
        invoice_ref: params[:invoice_ref],
        monthly_invoice: true
      })
    end

    if params[:smime_client_order]
      @return_url_params.merge!({
        certificate: params[:certificate],
        emails: params[:emails],
        smime_client_order: true
      })
    end
      
    return to_cents(@total), {
      ip:                request.remote_ip,
      return_url:        url_for(@return_url_params),
      cancel_return_url: root_url,
      subtotal:          to_cents(@total),
      shipping:          to_cents(@shipping),
      handling:          0,
      tax:               0,
      allow_note:        true,
      items:             @items,
    }
  end
  
  def get_domains_adjustments
    if params[:reprocess_ucc] || params[:renew_ucc] || params[:ucc_csr_submit]
      dom_params = {
        co_ref:             params[:co_ref],
        cc_ref:             params[:cc_ref],
        wildcard_amount:    params[:wildcard_amount],
        nonwildcard_amount: params[:nonwildcard_amount],
        wildcard_count:     params[:wildcard_count],
        nonwildcard_count:  params[:nonwildcard_count]
      }
      dom_params.merge!(reprocess_ucc: true) if params[:reprocess_ucc]
      dom_params.merge!(renew_ucc: true) if params[:renew_ucc]
      dom_params.merge!(ucc_csr_submit: true) if params[:ucc_csr_submit]
      @return_url_params.merge!(dom_params)
    end
  end
  
  def get_order_info(gateway_response, cart)
    subtotal, shipping, total = get_totals(cart)
    {
        shipping_address: gateway_response.address,
        email: gateway_response.email,
        name: gateway_response.name,
        gateway_details: {
            :token => gateway_response.token,
            :payer_id => gateway_response.payer_id,
        },
        subtotal: gateway_response.params['order_total'],
        shipping: gateway_response.params['shipping_total'],
        total: gateway_response.params['order_total']
    }
  end

  def get_shipping(cart)
    # define your own shipping rule based on your cart here
    # this method should return an integer
  end

  def get_items(cart)
    if params[:reprocess_ucc]
      [{
        name: "Reprocess UCC Cert",
        number: "reprocess order",
        quantity: 1,
        amount: get_amount(params[:amount])
      }]
    elsif params[:monthly_invoice]
      [{
        name: "Monthly Invoice Pmt",
        number: "payment",
        quantity: 1,
        amount: get_amount(params[:amount])
      }]
    elsif params[:renew_ucc]
      [{
        name: "Renew UCC Cert",
        number: "renewal order",
        quantity: 1,
        amount: get_amount(params[:amount])
      }]
    elsif params[:ucc_csr_submit]
      [{
        name: "UCC Cert Adjustment",
        number: "adjustment order",
        quantity: 1,
        amount: get_amount(params[:amount])
      }]
    elsif params[:smime_client_order]
      [{
        name: "S/MIME Client Enroll",
        number: "enrollment order",
        quantity: 1,
        amount: get_amount(params[:amount])
      }]
    else  
      cart.line_items.collect do |line_item|
        if line_item.sellable.is_a?(Deposit)
          {
              :name => "Deposit",
              :number => "sslcomdeposit"
          }
        elsif line_item.sellable.is_a?(ResellerTier)
          product = line_item.sellable
          {
              :name => product.roles,
              :number => product.roles
          }
        else
          product = line_item.sellable.is_a?(CertificateOrder) ? line_item.sellable.certificate : line_item.sellable
          {
              :name => product.title,
              :number => product.serial
          }
        end.merge(quantity: 1, amount: line_item.amount.cents )
      end
    end
  end

  def get_purchase_params(gateway_response, request, params)
    items = gateway_response.params['PaymentDetails']['PaymentDetailsItem']
    items=[items] unless items.is_a?(Array)
    new_items = items.map{|i|
      {amount: to_cents(i['Amount'].to_f * 100),
       name: i['Name'],
       quantity: i['Quantity'],
       Number: i['Number']}
    }
    return to_cents(gateway_response.params['order_total'].to_f * 100), {
        :ip => request.remote_ip,
        :token => gateway_response.token,
        :payer_id => gateway_response.payer_id,
        :subtotal => to_cents(gateway_response.params['order_total'].to_f * 100),
        :shipping => to_cents(gateway_response.params['shipping_total'].to_f * 100),
        :handling => 0,
        :tax =>      0,
        :items =>    new_items
    }
  end

  def get_totals(cart, params)
    subtotal = if params[:reprocess_ucc] || params[:monthly_invoice] || params[:smime_client_order]
      get_amount(params[:amount])
    else
      cart.amount.cents
    end
    discount = get_amount(params[:discount])
    credit   = get_amount(params[:funded_account])
    surplus  = get_surplus_deposit(params) > 0 ? get_surplus_deposit(params) : 0
    shipping = 0.0
    total    = (subtotal - credit - discount) + surplus + shipping
    return subtotal, shipping, total
  end

  def to_cents(money)
    (money*1).round
  end

  def credit_and_discount(params)
    funded_account_amt = get_amount(params[:funded_account])
    discount_amt       = get_amount(params[:discount])
    
    if params[:discount_code] && (discount_amt > 0)
      @items.push({
        name:     'Discount',
        number:   params[:discount_code],
        quantity: 1,
        amount:   -discount_amt
      })
    end
    # add credit from funded account towards purchase
    if params[:funded_account] && (funded_account_amt > 0)
      @items.push({
        name:     'Funded Account',
        number:   'Credit',
        quantity: 1,
        amount:   -funded_account_amt
      })
    end
  end

  def load_funds(params)
    # load additional (any amount above the purchase amount) to funded account
    if params[:funded_target] && (get_surplus_deposit(params) > 0)
      @items.push({
        name:     'Load Funds',
        number:   'to Funded Account',
        quantity: 1,
        amount:   get_surplus_deposit(params)
      })
    end
  end

  def get_surplus_deposit(params)
    special_order = params[:monthly_invoice] || params[:reprocess_ucc] || params[:smime_client_order]
    funded        = params[:funded_target]
    final_total   = params[:amount].to_i - get_amount(params[:discount]) - get_amount(params[:funded_account])
    (funded && !special_order) ? get_amount(funded) - final_total : 0
  end

  def get_amount(amount=nil)
    amount ? (amount.to_f * 100).round : 0
  end
end
