class CaaCheck < ApplicationRecord
  belongs_to :checkable, :polymorphic => true

  CAA_COMMAND=->(name, authority){
    %x{echo `cd #{Rails.application.secrets.caa_check_path} && python checkcaa.py #{name} #{authority}`}
  }

  CaaCheckJob = Struct.new(:certificate_order_id, :certificate_name, :certificate_content) do
    def perform
      name = certificate_name.name
      is_comodoca = certificate_content.nil? ? false : (certificate_content.ca.nil? ? false : true)

      if is_comodoca
        result = caa_lookup(name, comodo_ca_label)
        if result == true # Timeout
          return_obj = true
        elsif result =~ /status/ # Returned CAA Check Result.
          arry = JSON.parse(result.gsub("}\n", "}").gsub("\n", "|||"))
          log_caa_check(certificate_order_id, name, comodo_ca_label, arry)

          if arry['status'].to_s == 'true'
            return_obj = true
          elsif arry['status'].to_s == 'false'
            if arry['caatestout'].include? 'Failed to send CAA query to'
              return_obj = true
            elsif arry['caatestout'].include? 'not present in issue tag'
              return_obj = false
            end
          end
        else
          return_obj = false
        end
      else
        result = caa_lookup(name, ssl_ca_label)
        if result == true # Timeout
          return_obj = true
        elsif result =~ /status/ # Returned CAA Check Result.
          arry = JSON.parse(result.gsub("}\n", "}").gsub("\n", "|||"))
          log_caa_check(certificate_order_id, name, ssl_ca_label, arry)

          if arry['status'].to_s == 'true'
            return_obj = true
          elsif arry['status'].to_s == 'false'
            if arry['caatestout'].include? 'Failed to send CAA query to'
              return_obj = true
            elsif arry['caatestout'].include? 'not present in issue tag'
              return_obj = false
            end
          end
        else
          return_obj = false
        end
      end

      certificate_name.update_attribute(:caa_passed, return_obj)
    end

    def caa_lookup(name, authority)
      begin
        @checkcaa=IO.popen("echo `cd #{Rails.application.secrets.caa_check_path} && python checkcaa.py #{name} #{authority}`")
        result = @checkcaa.read
        Process.wait @checkcaa.pid
        result
      rescue RuntimeError
        return false
      rescue Exception=>e
        return false
      end
    end

    def log_caa_check(cert_order_ref, name, authority, result)
      dir = Rails.application.secrets.caa_check_log_path
      Dir.mkdir(dir) unless Dir.exists?(dir)

      caatestout = result['caatestout'].gsub("|||", "\n")
      message = result['message'].gsub("|||", "\n")

      log_path = (dir + '/' + cert_order_ref + '.txt').gsub('//', '/')
      file = File.open(log_path, 'a')
      file.write "**************************************** " + authority + " **************************************** \n"
      file.write "CAA " + authority + " check results for domain \"" + name + "\" at " + Time.now.strftime("%d/%m/%Y %H:%M:%S") + "\n"
      file.write "CAA " + authority + " test out : \n"
      file.write (caatestout + "\n").gsub("\n\n", "\n")
      file.write "Message : \n"
      file.write (message + "\n").gsub("\n\n", "\n")
      file.write "\n"
      file.close
    end
  end

  def self.pass?(_certificate_order_id, _certificate_name, _certificate_content)
    true # Delayed::Job.enqueue CaaCheckJob.new(certificate_order_id, certificate_name, certificate_content)
  end

  def ssl_ca_label
    I18n.t('labels.ssl_ca')
  end

  def comodo_ca_labl
    I18n.t('labels.comodo_ca')
  end
end
