 class Helpers
   include Singleton
   include ActionView::Helpers::TextHelper
   include ActionView::Helpers::UrlHelper
   include ActionView::Helpers::DateHelper
   include ActionView::Helpers::TagHelper
   include ActionView::Helpers::NumberHelper
 end