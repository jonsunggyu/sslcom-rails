json.extract! cdn, :id, :created_at, :updated_at
json.url cdn_url(cdn, format: :json)