# frozen_string_literal: true

Pillar::Core::Engine.routes.draw do
  root to: "application#show"
end
