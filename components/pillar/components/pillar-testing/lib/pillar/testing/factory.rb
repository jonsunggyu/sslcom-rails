# frozen_string_literal: true

require "factory_bot"
require "faker"

ActiveSupport.run_load_hooks(:factory_bot, FactoryBot)

require "factory_bot_rails"
