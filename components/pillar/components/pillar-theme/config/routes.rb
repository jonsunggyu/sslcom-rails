# frozen_string_literal: true

Pillar::Theme::Engine.routes.draw do
  root to: "application#show"
end
