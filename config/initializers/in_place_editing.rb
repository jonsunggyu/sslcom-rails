# frozen_string_literal: true

ActionController::Base.include InPlaceEditing
ActionController::Base.helper InPlaceMacrosHelper
